import axios from "axios"

function getURI(searchText){
    const postfix = (!!searchText ? searchText : "sky");
    return 'http://www.splashbase.co/api/v1/images/search?query=' + encodeURIComponent(postfix);
}

export default function loadImages(query){
    return axios.get(getURI(query));
}