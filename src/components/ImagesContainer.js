import React from "react";

export default function ImagesContainer(props){
    const handleOpenImageClick = (event) => {
        props.openLargeImageWindow(event.target.id);
    }

    return (
        <div className="col-8 mt-1">
            {
                props.imagesArray.map(
                    (image) => {
                        return (
                            <img 
                                key={image.id} 
                                id={image.id} 
                                className="col-3" 
                                src={image.url} 
                                onClick={handleOpenImageClick}
                            />
                        );
                    }
                )
            }
        </div>
    )
}