import React from "react";


export default function Button(props){
const clickHandler = () => {
    props.updateImageLoadingStatus({isImagesLoading: true});
    props.getImages();
}
    return (
        <button className="button" onClick={clickHandler}>Search</button>
    )
}