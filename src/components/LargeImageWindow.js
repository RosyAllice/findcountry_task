import React, {Component} from "react";

class LargeImageWindow extends Component{
    constructor(props){
        super(props);
        this.state = {
            currentImage: props.image
        };
    }

    render(){
        return (
            <div className="myModal">
                <img src={this.props.image.large_url} />
            </div>
        )
    }
}

export default LargeImageWindow;