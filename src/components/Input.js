import React from "react";

export default function Input(props){
    const inputHandler = (event) => {
        props.updateQuery({
            currentQuery: event.target.value
        });
    }

    return (
        <input className='col-3 mt-2' type="text" onInput={inputHandler}/>
    )
}