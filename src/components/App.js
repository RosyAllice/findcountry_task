import React, {Component} from "react";
import "bootstrap/dist/css/bootstrap.css";

import Input from "./Input";
import Button from "./Button";
import loadImages from "../services/imageLoadingService.js";
import ImagesContainer from "./ImagesContainer";
import LargeImageWindow from "./LargeImageWindow";

class App extends Component {
    state = {
        currentQuery: null,
        recievedImages: null, 
        isImagesLoading: false,
        largeImageToOpen: null
    }

    updateState = (partialState) => {
        this.setState(partialState);
    }

    getImages = () => {
        this.setState({isImagesLoading: true});
        loadImages(this.state.currentQuery)
        .then( (response) => {
            console.log("Images recieved");
            this.setState({
                recievedImages: response.data.images,
                isImagesLoading: false
            });
        })
        .catch( (error) => {
            console.log("error" + error.message);
        });
    }

    findImageById = (image_id) => {
        this.state.recievedImages.forEach(image => {
            if(image.id == image_id)
                return image;
        });
    }

    openLargeImageWindow = (image_id) => {
        const imageToOpen = this.state.recievedImages.find((imageElem) => {
            if(imageElem.id == image_id)
                return true;
            else
                return false;
        })
        this.setState({
            largeImageToOpen: imageToOpen 
        });
    }

    render(){
        const imagesContainer = !this.state.recievedImages 
            ? <div></div> 
            : <ImagesContainer imagesArray={this.state.recievedImages} openLargeImageWindow={this.openLargeImageWindow}/>;
        const imageWindow = !this.state.largeImageToOpen
            ? <div className="hidden"></div>
            : <LargeImageWindow image={this.state.largeImageToOpen} /> //modul-vindow

        return <div className="container col-10">
            <Input updateQuery={this.updateState}/>
            <Button getImages={this.getImages} updateImageLoadingStatus={this.updateState} />
            {imagesContainer}
            {imageWindow}
            {this.state.isImagesLoading && <div className="lds-ripple"><div></div><div></div></div>}
        </div>
    }
}

export default App;